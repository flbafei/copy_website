<?php

//主执行程序
function exc($get_str,$copy_period){
    //获取复制列表
    $get_arr=explode("|",$get_str[1]);
    //复制指针
    $start=$get_str[0];
    //长度
    $len=count($get_arr);
    //执行复制列表记录
    for ($i=$start; $i <$len ; $i++) { 
        $copy_status=explode("=>",$get_arr[$i]);
        $code=get_code($copy_status[0]); //获取动态码
        $code=replace_f($code); //替换
        $code=external_link($code[0],$code[1],$get_str,$copy_period); //链接处理
        $get_str[1]=$code[1][1];//新的复制列表
        $code=$code[0];
        save_file($copy_status[1],$code,$copy_period);//保存文件
        $get_str[0]++;//指针加1
    }
    //获取新的复制列表
    $get_arr=explode("|",$get_str[1]);
    $new_len=count($get_arr);
    if($new_len>$len){
        //当前复制级数是否小于定义复制级数
        if($copy_period<COPY_LAVEL){
            $copy_period++;
            exc($get_str,$copy_period);
        }
    }
}

//链接处理
function external_link($string,$record,$get_str,$copy_period){
    //清除外链js
    preg_match_all('/<script[^<]*?src="http[^<]*?.js"><\/script>/',$string,$rs);
    if($rs){
        foreach ($rs[0] as $value) {
            if(strpos($value,TARGET_DOMAIN.".com")===false){
                $string=str_replace($value,"",$string);
            }
        }
    }
    
    //a标签
    preg_match_all('/<a[^<]*?href="([^<]*?)"[^<]*?>/',$string,$rs);
    if($rs[1]){
        $white_list=explode(",",WHITE_LIST);
        for ($i=0; $i <count($rs[1]) ; $i++){
            //带http的外链
            if(strpos($rs[1][$i],"http")!==false){
                if(!ALLOW_EXTERNAL_LINK){
                    $mark=1;
                    if($copy_period<COPY_LAVEL){
                        ///含有目标域名的链接
                        if(strpos($rs[0][$i],TARGET_DOMAIN)!==false){
                            //如果该连接没有记录。则在获取复制列表中增加一条记录
                            if(!strpos($get_str[1],$rs[1][$i])){
                                $rs_str=$rs[1][$i];
                                $rs_str=str_replace("http://","",$rs_str);
                                $rs_str=str_replace("https://","",$rs_str);
                                $fiel_name=substr($rs_str,strpos($rs_str,"/")+1);
                                $get_str[1].="|".$rs_str."=>../".$fiel_name;//增加复制列表
                                $string=str_replace($rs_str,"/".$fiel_name,$string);//替换页面链接
                            }
                            $mark=0;
                        }
                        else{
                            //外连接是否在白名单
                            foreach ($white_list as $k) {
                                if(strpos($rs[0][$i],$k)){
                                    //如果该连接没有记录。则在获取列表中增加一条记录
                                    if(!strpos($get_str[1],$rs[1][$i])){
                                        $fiel_name=substr($rs[1][$i],strripos($rs[1][$i],"/")+1);
                                        $test="|".$rs[1][$i]."=>../".$k."/".$fiel_name;
                                        $get_str[1].="|".$rs[1][$i]."=>../".$k."/".$fiel_name;//增加复制列表
                                        $string=str_replace($rs[1][$i],"../".$k."/".$fiel_name,$string);//替换页面链接
                                    }
                                    $mark=0;
                                }
                            }
                        }
                    }
                    //再次过滤，
                    if($mark){
                        //外连接是否在替换记录中，则不用处理外链
                        foreach ($record as $k) {
                            if($k){
                                if(strpos($rs[0][$i],$k)){
                                    $mark=0;
                                }
                            }
                        }
                    }
                    //最终替换
                    if($mark){
                        $rs0=$rs[0][$i];
                        $rs0=str_replace($rs[1][$i],MY_LINK,$rs0);
                        $string=str_replace($rs[0][$i],$rs0,$string);
                    }
                }
            }
            //内部连接
            else{
                //如果该内部接没有记录。则增加复制列表
                $test=strpos($get_str[1],$rs[1][$i]);
                if(!strpos($get_str[1],$rs[1][$i])){
                    if(substr($rs[1][$i],0,1)=="/"){
                        $get_str[1].="|".TARGET_DOMAIN.".com".$rs[1][$i]."=>..".$rs[1][$i];
                    }
                    else{
                        $get_str[1].="|".TARGET_DOMAIN.".com".$rs[1][$i]."=>../".$rs[1][$i]; 
                    }

                }
            }
        }
    }
    $back=array($string,$get_str);
    return $back;
}
//保存文件
function save_file($url,$code,$copy_period){
    //状态进度汇报
    $ts_file=fopen("../ts.txt","w+");
    fwrite($ts_file,$url."start to save!/copy_period=".$copy_period);
    fclose($ts_file);

    $url=str_replace('\\',"/",$url);
    //分解路由以判断路由文件夹是否存在
    $folder=explode("/",$url);
    $folder_lavel=array();
    $folder_lavel[0]=$folder[0]."/";
    for ($i=1; $i <count($folder) ; $i++) { 
        $folder_lavel[$i]="";
        for ($j=0; $j <$i ; $j++) { 
            $folder_lavel[$i]=$folder[$j].$folder_lavel[$i]."/";
        }
        $folder_lavel[$i]=$folder_lavel[$i].$folder[$i];
    }
    for ($i=0; $i <count($folder_lavel)-1 ; $i++) { 
        //路由中某文件夹不存在则新建文件夹
        is_dir($folder_lavel[$i]) OR mkdir($folder_lavel[$i], 0777, true); 
    }
    //保存
    $code="\xEF\xBB\xBF".$code;
    $file=fopen($url,"w+");
    fwrite($file,$code);
    fclose($file);

}

//获取动态码
function get_code($url){
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_HEADER,0); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //不验证证书
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //不验证证书
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1 ); 
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
    $code = curl_exec($ch); 
    //设置编码方式
    $code = str_replace('gb2312','utf-8',$code);
    $code = mb_convert_encoding($code, 'utf-8', 'GBK,UTF-8,ASCII');
    return $code;
}
//替换
function replace_f($code){
    $record_arr=array();
    $record_num=0;
    $replace_arr=read_file("replace_str.txt");
    if($replace_arr){
        foreach ($replace_arr as $key) {
            $replace_str=$key;
            if($replace_str){
                $replace_str=explode("=>",$replace_str);
                if(count($replace_str)==1){
                    $replace_str[1]="";
                }
                $data=$replace_str[1];
                //远程数据处理
                preg_match_all('/{([\\s\\S]*?)}/',$data,$rs);
                if($rs[0]){
                    for ($i=0; $i <count($rs[1]) ; $i++) {
                        //获取远程数据
                        $remote_data=get_remote_data($rs[1][$i]);
                        $data=str_replace($rs[0][$i],$remote_data,$data);
                        //保存替换记录
                        $record_arr[$record_num]=$remote_data;
                        $record_num++;
                    }
                }
                else{
                    //保存替换记录
                    $record_arr[$record_num]=$data;
                    $record_num++;
                }
                $code=str_replace($replace_str[0],$data,$code);
            }
        }
        //替换本网址域名
        $code=str_replace("DOMIAN_NAME",DOMIAN_NAME,$code);
    }
    $get=array(0=>$code,1=>$record_arr);
    return $get;
}

//获取远程数据
function get_remote_data($string){
    $rs_arr=explode("/",$string);
    //获取远程路由
    $url=REMORE_URL.$rs_arr[0]."/";
    $type=$rs_arr[1];
    $data=$rs_arr[2];
    switch ($type) {
        case 'img':
            $get=$url.$data;
            break;
        case 'txt':
            $txt_arr=read_file($url.TXT);
            $get=$data;
            foreach ($txt_arr as $key) {
                $remote_data=explode("=>",$key);
                if($remote_data[0]==$data){
                    $get=$remote_data[1];
                }
            }
            break;
        default:
            break;
    }
    return $get;
}
//去除注释
function remove_note($string){
    if(strpos($string,"////")!==false){
        $get=substr($string,0,strpos($string,"////"));
    }
    else{
        $get=$string;
    }
    $get=trim($get);
    return $get;
}
//读取文件
function read_file($url){
    $file=fopen($url,"r");
    $i=0;
    while (!feof($file)) {
        $arr[$i]=trim(fgets($file));
        $arr[$i]=remove_note($arr[$i]);
        $i++;
    }
    return $arr;
}