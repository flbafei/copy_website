<?php

//外部链接处理
function external_link($string,$record){
    if(!ALLOW_EXTERNAL_LINK){
        //获取a标签外连接
        preg_match_all('/<a[^<]*?href="(http[^<]*?)"[^<]*?>/',$string,$rs);
        if($rs[1]){
            for ($i=0; $i <count($rs[1]) ; $i++) { 
                $white_list=WHITE_LIST;
                $white_list=explode(",",$white_list);
                $mark=1;
                //外连接是否在白名单
                foreach ($white_list as $k) {
                    if(strpos($rs[0][$i],$k)){
                        $mark=0;
                    }
                }
                if($mark){
                    //外连接是否在替换记录中
                    foreach ($record as $k) {
                        if($k){
                            if(strpos($rs[0][$i],$k)){
                                $mark=0;
                            }
                        }
                    }
                }
                if($mark){
                    $rs0=$rs[0][$i];
                    $rs0=str_replace($rs[1][$i],MY_LINK,$rs0);
                    $string=str_replace($rs[0][$i],$rs0,$string);
                }
            }
        }
    }
    return $string;
}

//获取动态码
function get_code($url){
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_HEADER,0); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //不验证证书
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //不验证证书
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1 ); 
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
    $code = curl_exec($ch); 
    //设置编码方式
    $code = str_replace('gb2312','utf-8',$code);
    $code = mb_convert_encoding($code, 'utf-8', 'GBK,UTF-8,ASCII');
    return $code;
}
//替换
function replace_f($code){
    $record_arr=array();
    $record_num=0;
    $replace_arr=read_file("replace_str.txt");
    if($replace_arr){
        foreach ($replace_arr as $key) {
            $replace_str=$key;
            if($replace_str){
                //替换
                $replace_str=explode("=>",$replace_str);
                if(count($replace_str)==1){
                    $replace_str[1]="";
                }
                $data=$replace_str[1];
                //远程数据处理
                preg_match_all('/{([\\s\\S]*?)}/',$data,$rs);
                if($rs[0]){
                    for ($i=0; $i <count($rs[1]) ; $i++) { 
                        $remote_data=get_remote_data($rs[1][$i]);
                        $data=str_replace($rs[0][$i],$remote_data,$data);
                        $record_arr[$record_num]=$remote_data;
                        $record_num++;
                    }
                }
                else{
                    $record_arr[$record_num]=$data;
                    $record_num++;
                }

                $code=str_replace($replace_str[0],$data,$code);
            }
        }
        $code=str_replace("DOMIAN_NAME",DOMIAN_NAME,$code);
    }
    $get=array(0=>$code,1=>$record_arr);
    return $get;
}

//获取远程数据
function get_remote_data($string){
    $rs_arr=explode("/",$string);
    $url=REMORE_URL.$rs_arr[0]."/";
    $type=$rs_arr[1];
    $data=$rs_arr[2];
    switch ($type) {
        case 'img':
            $get=$url.$data;
            break;
        case 'txt':
            $txt_arr=read_file($url.TXT);
            $get=$data;
            foreach ($txt_arr as $key) {
                $remote_data=explode("=>",$key);
                if($remote_data[0]==$data){
                    $get=$remote_data[1];
                }
            }
            break;
        default:
            break;
    }
    return $get;
}
//去除注释
function remove_note($string){
    if(strpos($string,"#")!==false){
        $get=substr($string,0,strpos($string,"#"));
    }
    else{
        $get=$string;
    }
    $get=trim($get);
    return $get;
}
//读取文件
function read_file($url){
    $file=fopen($url,"r");
    $i=0;
    while (!feof($file)) {
        $arr[$i]=trim(fgets($file));
        $arr[$i]=remove_note($arr[$i]);
        $i++;
    }
    return $arr;
}